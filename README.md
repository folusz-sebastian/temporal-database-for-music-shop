# SQL Server bitemporal sample database

##  Opis projektu
Projekt przedstawia model bitemporalnej bazy danych przygotowanej w środowisku SQL Server. W ramach projektu został przygotowany również zestaw przykładowych danych.

## Opis bazy danych
Baza danych przechowuje dane sklepu internetowego, który zajmuje się sprzedażą płyt.
Opis tabel:

- Artist - tabela przechowująca informacje o artyście – może być nim zarów-no muzyk wchodzący w skład zespołu, jak i wokalista (informuje o tym ko-lumna isVocalist),
- ArtistInstrument – tabela temporalna przechowująca informacje o przypisa-niu artysty do instrumentu. Aspekt temporalności w tabeli polega na tym, że artysta w czasie trwania swojej kariery może zacząć grę na różnych instru-mentach (może to zależeć od rodzaju zespołu, do którego należy). Kolumny „ValidStart” oraz „ValidEnd” przechowują informację o tym, w jakim cza-sie dany artysta grał na instrumencie,
- Band – zespół, którego album(płytę) możemy nabyć,
- ArtistBand - tabela temporalna przechowująca informacje o przypisaniu ar-tysty do zespołu. Aspekt temporalności w tabeli polega na tym, że artysta w czasie trwania swojej kariery może należeć do różnych zespołów. Czas przebywania artysty w danym zespole reprezentowany jest przez kolumny „ValidStart” oraz „ValidEnd”,
- Album – album, który może nabyć klient w ramach działalności sklepu,
- Client – tabela przechowująca informacje o kliencie, który może dokonać zamówienia,
- Order – tabela temporalna przechowująca informacje o zamówieniu albumu przez klienta. Aspekt temporalności w tabeli polega na tym, że zamówienie złożone przez klienta może zostać anulowane lub zmienione, czego pełna historia będzie przechowywana. Datę złożenia zamówienia przechowują kolumny ValidStart oraz ValidEnd.

Dodatkowo, w celu zachowania historii zmian tabel temporalnych zostały utworzone tabele ArtistInstrumentHistory, ArtistBandHistory oraz OrderHistory. 

Diagram encji został przedstawiony na rys. 1.

![entity_relationship_diagram.JPG](entity_relationship_diagram.JPG)

rys. 1. Diagram encji